<?php
try {
    require_once('includes/funciones/bd_conexion.php');
    $sql = "SELECT * FROM `tbl_invitados`";
    $resultado = $conn->query($sql);
} catch (Exception $e) {
    echo $e->getMessage();
}
?>

    <section class="invitados contenedor seccion">
        <h2>Invitados</h2>
        <ul class="lista-ivitados clearfix">
            <?php while ($invitados = $resultado->fetch_assoc()) { ?>
                <li>
                    <div class="invitado">
                        <a class="invitado-info" href="#invitado<?php echo $invitados['idInvitado']; ?>">
                            <img src="img/<?php echo $invitados['urlImagen'] ?>" alt="imagen invitado">
                            <p><?php echo $invitados['nombre'] . " " . $invitados['apellido'] ?></p>
                        </a>
                    </div>
                </li>
                <div style="display: none;">
                    <div class="invitado-info" id="invitado<?php echo $invitados['idInvitado']; ?>">
                        <h2><?php echo $invitados['nombre'] . " " . $invitados['apellido'] ?></h2>
                        <img src="img/<?php echo $invitados['urlImagen'] ?>" alt="imagen invitado">
                        <p><?php echo $invitados['descripcion']; ?></p>
                    </div>
                </div>
            <?php } ?>
        </ul><!--.lista-invitados-->
    </section><!--.invitados-->

<?php
$conn->close();
?>