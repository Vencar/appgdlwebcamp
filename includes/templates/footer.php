<footer class="site-footer">
    <div class="contenedor clearfix">
        <div class="footer-informacion">
            <h3>Sobre <span>uagrowebcamp</span></h3>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Sed malesuada ante quis lectus aliquet, vel scelerisque nisl vestibulum.
                Maecenas a dolor auctor erat lobortis tristique.
            </p>
        </div>
        <div class="ultimos-tweets">
            <h3>Últimos <span>tweets</span></h3>
            <ul>
                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Sed malesuada ante quis lectus aliquet.
                </li>
                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Sed malesuada ante quis lectus aliquet.
                </li>
                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Sed malesuada ante quis lectus aliquet.
                </li>
            </ul>
        </div>
        <div class="menu">
            <h3>Redes <span>sociales</span></h3>
            <nav class="redes-sociales">
                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
            </nav><!--.redes-sociales-->
        </div>
    </div>
</footer>

<p class="copyright">Todos los derechos reservados UAGROWEBCAMP 2017</p>

<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
<script src="js/jquery.animateNumber.js"></script>
<script src="js/jquery.lettering.js"></script>
<script src="js/jquery.countdown.js"></script>
<script src="js/jquery.waypoints.js"></script>
<?php
$archivo = basename($_SERVER['PHP_SELF']);
$pagina = str_replace(".php", "", $archivo);
if ($pagina == 'invitados' || $pagina == 'index') {
    echo '<script src="js/jquery.colorbox-min.js"></script>';
} else if ($pagina == 'conferencia') {
    echo '<script src="js/lightbox.js"></script>';
}
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAUQYdydIsP6MtIKoZn85A3ruO4HpIyx1s&callback=initMap"
        async defer></script>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function (b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
            function () {
                (b[l].q = b[l].q || []).push(arguments)
            });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = 'https://www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-XXXXX-X', 'auto');
    ga('send', 'pageview');
</script>
</body>
</html>
