<?php include_once 'includes/templates/header.php' ?>

    <section class="seccion contenedor">
        <h2>La mejor conferencia de diseño web en español</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed malesuada ante quis lectus aliquet, vel
            scelerisque nisl vestibulum. Maecenas a dolor auctor erat lobortis tristique. Etiam sed odio molestie,
            gravida mi et, semper metus. Suspendisse tempus velit nec neque fermentum, eget placerat ipsum porttitor.
            Aenean fermentum erat vel magna aliquam fringilla. Donec quis elementum nibh, et elementum enim. Donec sed
            mi quam. Ut ac consequat leo. Curabitur nec bibendum sem. Nullam quam nibh, consectetur eget eleifend eu,
            faucibus non nisi. Proin egestas sollicitudin metus a faucibus. Sed in finibus lectus.</p>
    </section><!--.seccion contenedor-->

    <section class="programa">

        <div class="contenedor-video">
            <video autoplay loop poster="img/bg-talleres.jpg">
                <source src="video/video.mp4" type="video/mp4">
                <source src="video/video.webm" type="video/webm">
                <source src="video/video.ogv" type="video/ogg">
            </video>
        </div><!--.contenedor-video-->

        <div class="contenido-programa">
            <div class="contenedor">
                <div class="programa-evento">
                    <h2>Programa del Evento</h2>
                    <?php
                    try {
                        require_once('includes/funciones/bd_conexion.php');
                        $sql = "SELECT * FROM tbl_categoria_evento";
                        $resultado = $conn->query($sql);
                    } catch (Exception $e) {
                        echo $e->getMessage();
                    }
                    ?>
                    <nav class="menu-programa">
                        <?php while ($cat = $resultado->fetch_array(MYSQLI_ASSOC)) { ?>
                            <?php $categoria = $cat['catEvento']; ?>
                            <a href="#<?php echo strtolower($categoria) ?>">
                                <i class="fa <?php echo $cat['icono'] ?>"
                                   aria-hidden="true"></i><?php echo $categoria ?>
                            </a>
                        <?php } ?>
                    </nav><!--.menu-programa-->

                    <?php
                    try {
                        require_once('includes/funciones/bd_conexion.php');
                        $sql = "SELECT idEvento, nombreEvento, fechaEvento, horaEvento, catEvento, icono, nombre, apellido ";
                        $sql .= "FROM tbl_eventos ";
                        $sql .= "INNER JOIN tbl_categoria_evento ";
                        $sql .= "ON tbl_eventos.idCategoriaEvento = tbl_categoria_evento.idCategoria ";
                        $sql .= "INNER JOIN tbl_invitados ";
                        $sql .= "ON tbl_eventos.idInvitado = tbl_invitados.idInvitado ";
                        $sql .= "AND tbl_eventos.idCategoriaEvento = 1 ";
                        $sql .= "ORDER BY idEvento LIMIT 2; ";
                        $sql .= "SELECT idEvento, nombreEvento, fechaEvento, horaEvento, catEvento, icono, nombre, apellido ";
                        $sql .= "FROM tbl_eventos ";
                        $sql .= "INNER JOIN tbl_categoria_evento ";
                        $sql .= "ON tbl_eventos.idCategoriaEvento = tbl_categoria_evento.idCategoria ";
                        $sql .= "INNER JOIN tbl_invitados ";
                        $sql .= "ON tbl_eventos.idInvitado = tbl_invitados.idInvitado ";
                        $sql .= "AND tbl_eventos.idCategoriaEvento = 2 ";
                        $sql .= "ORDER BY idEvento LIMIT 2; ";
                        $sql .= "SELECT idEvento, nombreEvento, fechaEvento, horaEvento, catEvento, icono, nombre, apellido ";
                        $sql .= "FROM tbl_eventos ";
                        $sql .= "INNER JOIN tbl_categoria_evento ";
                        $sql .= "ON tbl_eventos.idCategoriaEvento = tbl_categoria_evento.idCategoria ";
                        $sql .= "INNER JOIN tbl_invitados ";
                        $sql .= "ON tbl_eventos.idInvitado = tbl_invitados.idInvitado ";
                        $sql .= "AND tbl_eventos.idCategoriaEvento = 3 ";
                        $sql .= "ORDER BY idEvento LIMIT 2;";
                    } catch (Exception $e) {
                        echo $e->getMessage();
                    }
                    ?>

                    <?php $conn->multi_query($sql); ?>
                    <?php
                    do {
                        $resultado = $conn->store_result();
                        $row = $resultado->fetch_all(MYSQLI_ASSOC); ?>
                        <?php $i = 0; ?>
                        <?php foreach ($row as $evento): ?>
                            <?php if ($i % 2 == 0) { ?>
                                <div id="<?php echo strtolower($evento['catEvento']); ?>" class="info-curso ocultar clearfix">
                            <?php } ?>
                            <div class="detalle-evento">
                                <h3><?php echo $evento['nombreEvento']; ?></h3>
                                <p><i class="fa fa-clock-o" aria-hidden="true"></i><?php echo $evento['horaEvento']; ?>
                                </p>
                                <p><i class="fa fa-calendar"
                                      aria-hidden="true"></i><?php echo $evento['fechaEvento']; ?></p>
                                <p><i class="fa fa-user"
                                      aria-hidden="true"></i><?php echo $evento['nombre'] . " " . $evento['apellido']; ?>
                                </p>
                            </div>
                            <?php if ($i % 2 == 1): ?>
                                <a href="calendario.php" class="button float-rigth">Ver Todos</a>
                                </div><!--#talleres-->
                            <?php endif; ?>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                        <?php $resultado->free(); ?>
                    <?php } while ($conn->more_results() && $conn->next_result()); ?>
                </div><!--.programa-evento-->
            </div><!--.contenedor-->
        </div><!--.contenido-programa-->
    </section><!--programa-->

<?php include_once 'includes/templates/invitados.php' ?>

    <div class="contador parallax">
        <div class="contenedor">
            <ul class="resumen-evento clearfix">
                <li><p class="numero"></p>Invitados</li>
                <li><p class="numero"></p>Talleres</li>
                <li><p class="numero"></p>Días</li>
                <li><p class="numero"></p>Conferencias</li>
            </ul>
        </div><!--.contenedor-->
    </div><!--.contador parallex-->

    <section class="precios seccion">
        <h2>Precios</h2>
        <div class="contenedor">
            <ul class="lista-precios clearfix">
                <li>
                    <div class="tabla-precio">
                        <h3>Pase por día</h3>
                        <p class="numero">$30</p>
                        <ul>
                            <li>Bocadillos Gratis</li>
                            <li>Todas las conferencias</li>
                            <li>Todos los talleres</li>
                        </ul>
                        <a href="#" class="button hollow">Comprar</a>
                    </div>
                </li>
                <li>
                    <div class="tabla-precio">
                        <h3>Todos los dias</h3>
                        <p class="numero">$50</p>
                        <ul>
                            <li>Bocadillos Gratis</li>
                            <li>Todas las conferencias</li>
                            <li>Todos los talleres</li>
                        </ul>
                        <a href="#" class="button">Comprar</a>
                    </div>
                </li>
                <li>
                    <div class="tabla-precio">
                        <h3>Pase por 2 días</h3>
                        <p class="numero">$45</p>
                        <ul>
                            <li>Bocadillos Gratis</li>
                            <li>Todas las conferencias</li>
                            <li>Todos los talleres</li>
                        </ul>
                        <a href="#" class="button hollow">Comprar</a>
                    </div>
                </li>
            </ul>
        </div><!--.contenedor-->
    </section><!--.precios seccion-->

    <div id="mapa" class="mapa">

    </div>

    <section class="seccion">
        <h2>Testimoniales</h2>
        <div class="testimoniales contenedor clearfix">
            <div class="testimonial">
                <blockquote>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Sed malesuada ante quis lectus aliquet, vel scelerisque nisl vestibulum.
                        Maecenas a dolor auctor erat lobortis tristique.
                    </p>
                    <footer class="info-testimonial clearfix">
                        <img src="img/testimonial.jpg" alt="imagen testimonial">
                        <cite>
                            Oswaldo Aponte Escobedo <span>Diseñador en @prisma</span>
                        </cite>
                    </footer>
                </blockquote>
            </div><!--.testimonial-->
            <div class="testimonial">
                <blockquote>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Sed malesuada ante quis lectus aliquet, vel scelerisque nisl vestibulum.
                        Maecenas a dolor auctor erat lobortis tristique.
                    </p>
                    <footer class="info-testimonial clearfix">
                        <img src="img/testimonial.jpg" alt="imagen testimonial">
                        <cite>
                            Oswaldo Aponte Escobedo <span>Diseñador en @prisma</span>
                        </cite>
                    </footer>
                </blockquote>
            </div><!--.testimonial-->
            <div class="testimonial">
                <blockquote>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Sed malesuada ante quis lectus aliquet, vel scelerisque nisl vestibulum.
                        Maecenas a dolor auctor erat lobortis tristique.
                    </p>
                    <footer class="info-testimonial clearfix">
                        <img src="img/testimonial.jpg" alt="imagen testimonial">
                        <cite>
                            Oswaldo Aponte Escobedo <span>Diseñador en @prisma</span>
                        </cite>
                    </footer>
                </blockquote>
            </div><!--.testimonial-->
        </div><!--.testimoniales .contenedor .clearfix-->
    </section>

    <div class="newsletter parallax">
        <div class="contenido contenedor">
            <p>regístrate al newsletter</p>
            <h3>Gdlwebcapm</h3>
            <a href="#" class="button transparente">Registro</a>
        </div><!-- .contenido contenedor-->
    </div><!--.newsletter-->

    <section class="seccion">
        <h2>Faltan</h2>
        <div class="cuenta-regresiva contenedor">
            <ul class="clearfix">
                <li><p id="dias" class="numero">0</p>Dias</li>
                <li><p id="horas" class="numero">0</p>Horas</li>
                <li><p id="minutos" class="numero">0</p>Minutos</li>
                <li><p id="segundos" class="numero">0</p>Segundos</li>
            </ul>
        </div><!--.cuenta-regresiva-->
    </section>

<?php include_once 'includes/templates/footer.php' ?>