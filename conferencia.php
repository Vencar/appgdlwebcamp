<?php include_once 'includes/templates/header.php'?>

<section class="seccion contenedor">
    <h2>La mejor conferencia de diseño web en español</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed malesuada ante quis lectus aliquet, vel scelerisque nisl vestibulum. Maecenas a dolor auctor erat lobortis tristique. Etiam sed odio molestie, gravida mi et, semper metus. Suspendisse tempus velit nec neque fermentum, eget placerat ipsum porttitor. Aenean fermentum erat vel magna aliquam fringilla. Donec quis elementum nibh, et elementum enim. Donec sed mi quam. Ut ac consequat leo. Curabitur nec bibendum sem. Nullam quam nibh, consectetur eget eleifend eu, faucibus non nisi. Proin egestas sollicitudin metus a faucibus. Sed in finibus lectus.</p>
</section><!--.seccion contenedor-->

<section class="seccion contenedor">
    <h2>Galería de fotos</h2>
    <div class="galeria">
        <a href="img/galeria/01.jpg" data-lightbox="galeria">
            <img src="img/galeria/thumbs/01.jpg">
        </a>
        <a href="img/galeria/02.jpg" data-lightbox="galeria">
            <img src="img/galeria/thumbs/02.jpg">
        </a>
        <a href="img/galeria/03.jpg" data-lightbox="galeria">
            <img src="img/galeria/thumbs/03.jpg">
        </a>
        <a href="img/galeria/04.jpg" data-lightbox="galeria">
            <img src="img/galeria/thumbs/04.jpg">
        </a>
        <a href="img/galeria/05.jpg" data-lightbox="galeria">
            <img src="img/galeria/thumbs/05.jpg">
        </a>
        <a href="img/galeria/06.jpg" data-lightbox="galeria">
            <img src="img/galeria/thumbs/06.jpg">
        </a>
        <a href="img/galeria/07.jpg" data-lightbox="galeria">
            <img src="img/galeria/thumbs/07.jpg">
        </a>
        <a href="img/galeria/08.jpg" data-lightbox="galeria">
            <img src="img/galeria/thumbs/08.jpg">
        </a>
        <a href="img/galeria/09.jpg" data-lightbox="galeria">
            <img src="img/galeria/thumbs/09.jpg">
        </a>
        <a href="img/galeria/10.jpg" data-lightbox="galeria">
            <img src="img/galeria/thumbs/10.jpg">
        </a>

    </div>
</section>

<?php include_once 'includes/templates/footer.php'?>