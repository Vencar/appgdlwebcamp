<?php include_once 'includes/templates/header.php' ?>

    <section class="seccion contenedor">
        <h2>Calendario de Eventos</h2>

        <?php
        try {
            require_once('includes/funciones/bd_conexion.php');
            $sql = "SELECT idEvento, nombreEvento, fechaEvento, horaEvento, catEvento, icono, nombre, apellido ";
            $sql .= "FROM tbl_eventos ";
            $sql .= "INNER JOIN tbl_categoria_evento ";
            $sql .= "ON tbl_eventos.idCategoriaEvento = tbl_categoria_evento.idCategoria ";
            $sql .= "INNER JOIN tbl_invitados ";
            $sql .= "ON tbl_eventos.idInvitado = tbl_invitados.idInvitado ";
            $sql .= "ORDER BY idEvento";
            $resultado = $conn->query($sql);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        ?>

        <div class="calendario">
            <?php
            $calendario = array();
            while ($eventos = $resultado->fetch_assoc()) {
                //Obtener fecha del evento

                $fecha = $eventos['fechaEvento'];
                $evento = array(
                    'titulo' => $eventos['nombreEvento'],
                    'fecha' => $eventos['fechaEvento'],
                    'hora' => $eventos['horaEvento'],
                    'categoria' => $eventos['catEvento'],
                    'icono' => 'fa' . " " . $eventos['icono'],
                    'invitado' => $eventos['nombre'] . " " . $eventos['apellido']
                );
                $calendario[$fecha][] = $evento;
                ?>


            <?php } //while ?>
            <?php
            //Imprimir todos los eventos
            foreach ($calendario as $dia => $lista_eventos) { ?>
                <h3>
                    <i class="fa fa-calendar">
                        <?php
                        setlocale(LC_TIME, 'spanish');
                        echo utf8_encode(strftime("%A, %d, de %B del %Y", strtotime($dia)));
                        ?>
                    </i>
                </h3>
                <?php
                foreach ($lista_eventos as $evento) { ?>
                    <div class="dia">
                        <p class="titulo"><?php echo $evento['titulo']; ?></p>
                        <p class="hora">
                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                            <?php echo $evento['fecha'] . " " . $evento['hora']; ?>
                        </p>
                        <p>
                            <i class="<?php echo $evento['icono'] ?>" aria-hidden="true"></i>
                            <?php echo $evento['categoria']; ?>
                        </p>
                        <p>
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <?php echo $evento['invitado']; ?>
                        </p>
                    </div>
                <?php } //eventos ?>
            <?php } //dias ?>
        </div><!--.calendario-->
        <?php
        $conn->close();
        ?>

    </section>

<?php include_once 'includes/templates/footer.php' ?>